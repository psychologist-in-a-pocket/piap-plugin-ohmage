Library installation
-

Below are the git repos needed to use the Ohmage Android Library. As I have a defined folder structure 
for Project namings, you have to fix the Android Library references to the respective names.

    cd .../path/.../to/.../workspace/
    # checkout:
    git clone --recursive https://github.com/ohmage/ohmageAndroidLib.git Android_OhmageLib
    # which on :
    git clone --recursive https://github.com/cens/LogProbe.git Android_OhamgeLogProbe
    # which on :
    git clone --recursive https://github.com/cens/ohmageProbeLibrary.git Android_OhmageProbeLib

Running the Ohmage Server: Follow their tutorial with Ubuntu Precise. 
    
Installing the schema on the server: [you somehow have to obtain an auth token, use the token displayed by the MainActivity]
	
    curl -v -F "auth_token=e5e8d678-b2bb-429b-a978-dd9497394d4d" 
    	    -F "client=piap" 
    	    -F "observer_definition=@de.winteger.piap.schema.xml;type=text/xml" 
    	        http://192.168.178.78/app/observer/create
