package de.winteger.piap.plugin.ohmage;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import de.winteger.piap.consts.PiapIntentConsts;
import de.winteger.piap.ipc.IProtoMessageParser;
import de.winteger.piap.ipc.IProtoMessageReceiver;
import de.winteger.piap.ipc.ProtoIPCClient;
import de.winteger.piap.plugin.ohmage.PiapOhmageWrapper.ReturnState;
import de.winteger.piap.proto.Messages;
import de.winteger.piap.proto.Messages.InputLog;
import de.winteger.piap.proto.Messages.LocationLog;
import de.winteger.piap.proto.Messages.MoodLog;
import de.winteger.piap.proto.Messages.RootMessage;

public class MainActivity extends Activity {
	private Context ctx = this;
	private Activity act = this;
	private TextView tvInfo = null;

	private String socketName = null;
	private ProtoIPCClient<RootMessage> ipcClient;

	private IProtoMessageReceiver<Messages.RootMessage> messageReceiveCallback = new IProtoMessageReceiver<Messages.RootMessage>() {

		@Override
		public void onReceive(RootMessage mob) {
			if (mob == null) {
				return;
			}
			pushRootMessageToOhmage(mob);
		}

	};

	private IProtoMessageParser<Messages.RootMessage> messageParserCallback = new IProtoMessageParser<Messages.RootMessage>() {

		@Override
		public RootMessage parseMessageFromStream(InputStream in) throws IOException {
			return RootMessage.parseDelimitedFrom(in);

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Intent intent = getIntent();
		if (intent != null) {
			socketName = intent.getStringExtra(PiapIntentConsts.INTENT_EXTRA_SOCKET_NAME);
			if (socketName == null || socketName.equals("")) {
				// nothing to do here
				finish();
			}
		}

		tvInfo = (TextView) findViewById(R.id.tvInfo);

		ipcClient = new ProtoIPCClient<Messages.RootMessage>(socketName, messageReceiveCallback, messageParserCallback);
		tvInfo.setText("Connecting to Main App");
		ipcClient.connect();
	}

	@Override
	protected void onDestroy() {
		if (ipcClient != null) {
			ipcClient.close();
		}
		super.onDestroy();
	}

	//	@Override
	//	public boolean onCreateOptionsMenu(Menu menu) {
	//		// Inflate the menu; this adds items to the action bar if it is present.
	//		getMenuInflater().inflate(R.menu.main, menu);
	//		return true;
	//	}

	@SuppressWarnings("unused")
	private void connect() {
		PiapOhmageWrapper.putLocationLog(ctx, System.currentTimeMillis(), true);
		PiapOhmageWrapper.putMoodLog(ctx, System.currentTimeMillis(), "BAD_MOOD", 1337);
		PiapOhmageWrapper.putUserTextInputLog(ctx, System.currentTimeMillis(), "I hate debugging", "org.eclipse");
	}

	private void updateInfoText(final String s) {
		Runnable r = new Runnable() {

			@Override
			public void run() {
				tvInfo.setText(s);
			}
		};

		act.runOnUiThread(r);
	}

	private void pushRootMessageToOhmage(RootMessage mob) {
		updateInfoText("Receiving input logs");

		int x = 0;
		int xt = 0;

		for (InputLog il : mob.getInputLogList()) {
			xt++;
			if (il != null) {
				ReturnState s = PiapOhmageWrapper.putUserTextInputLog(ctx, il.getTimestamp(), il.getText(), il.getApp());
				if (s == ReturnState.ALL_OK) {
					x++;
				}
			}
		}

		updateInfoText("Receiving mood logs");

		int y = 0;
		int yt = 0;
		for (MoodLog ml : mob.getMoodLogList()) {
			yt++;
			if (ml != null) {
				ReturnState s = PiapOhmageWrapper.putMoodLog(ctx, ml.getTimestamp(), ml.getCategory(), ml.getCount());
				if (s == ReturnState.ALL_OK) {
					y++;
				}
			}
		}

		updateInfoText("Receiving location logs");

		int z = 0;
		int zt = 0;
		for (LocationLog ll : mob.getLocationLogList()) {
			zt++;
			if (ll != null) {
				ReturnState s = PiapOhmageWrapper.putLocationLog(ctx, ll.getTimestamp(), ll.getEntering());
				if (s == ReturnState.ALL_OK) {
					z++;
				}
			}
		}

		updateInfoText(String.format("Done\nPushed %d/%d input logs\nPushed %d/%d mood logs\nPushed %d/%d location logs", x, xt, y, yt, z, zt));
		ipcClient.close();
	}
}
