package de.winteger.piap.plugin.ohmage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;
import org.ohmage.probemanager.ProbeBuilder;
import org.ohmage.probemanager.ProbeWriter;

import android.content.Context;
import android.os.RemoteException;

public class PiapOhmageWrapper {
	public class C {
		public static final String CLIENT = "piap";
		
		public static final String OBSERVER_DEFINITION = "de.winteger.piap.ohmage";
		public static final int OBSERVER_VERSION = 1;

		public static final String STREAM_LOCATION_ID = "locationLog";
		public static final int STREAM_LOCATION_VERSION = 1;
		public static final String STREAM_LOCATION_DATE = "date";
		public static final String STREAM_LOCATION_ENTERING = "entering";

		public static final String STREAM_MOOD_ID = "moodLog";
		public static final int STREAM_MOOD_VERSION = 1;
		public static final String STREAM_MOOD_DATE = "date";
		public static final String STREAM_MOOD_CATEGORY = "mood";
		public static final String STREAM_MOOD_COUNT = "count";

		public static final String STREAM_USERINPUT_ID = "userInputLog";
		public static final int STREAM_USERINPUT_VERSION = 1;
		public static final String STREAM_USERINPUT_DATE = "date";
		public static final String STREAM_USERINPUT_TEXT = "text";
		public static final String STREAM_USERINPUT_APP = "app";

	}

	public enum ReturnState {
		JSON_EXCEPTION, OHMAGE_REMOTE_EXCEPTION, ALL_OK
	}

	public static ReturnState putLocationLog(Context ctx, long timestampNow, boolean entering) {
		throwIfNull(ctx);

		ProbeBuilder pb = new ProbeBuilder(C.OBSERVER_DEFINITION, C.OBSERVER_VERSION);
		pb.setStream(C.STREAM_LOCATION_ID, C.STREAM_LOCATION_VERSION);

		JSONObject data = new JSONObject();

		try {
			data.put(C.STREAM_LOCATION_DATE, getDateISO8601Seconds(timestampNow));
			data.put(C.STREAM_LOCATION_ENTERING, entering);
		} catch (JSONException e) {
			e.printStackTrace();
			return ReturnState.JSON_EXCEPTION;
		}
		pb.setData(data.toString());
		pb.withId().now();

		ProbeWriter var = new ProbeWriter(ctx);
		try {
			pb.write(var);
			var.close();
		} catch (RemoteException e) {
			e.printStackTrace();
			return ReturnState.OHMAGE_REMOTE_EXCEPTION;
		}

		return ReturnState.ALL_OK;
	}

	public static ReturnState putMoodLog(Context ctx, long timestampDay, String category, int count) {
		throwIfNull(ctx);

		ProbeBuilder pb = new ProbeBuilder(C.OBSERVER_DEFINITION, C.OBSERVER_VERSION);
		pb.setStream(C.STREAM_MOOD_ID, C.STREAM_MOOD_VERSION);

		JSONObject data = new JSONObject();

		try {
			data.put(C.STREAM_MOOD_DATE, getDateISO8601Day(timestampDay));
			data.put(C.STREAM_MOOD_CATEGORY, category);
			data.put(C.STREAM_MOOD_COUNT, count);
		} catch (JSONException e) {
			e.printStackTrace();
			return ReturnState.JSON_EXCEPTION;
		}
		pb.setData(data.toString());
		pb.withId().now();

		ProbeWriter var = new ProbeWriter(ctx);
		try {
			pb.write(var);
			var.close();
		} catch (RemoteException e) {
			e.printStackTrace();
			return ReturnState.OHMAGE_REMOTE_EXCEPTION;
		}
		return ReturnState.ALL_OK;
	}

	public static ReturnState putUserTextInputLog(Context ctx, long timestampNow, String text, String app) {
		throwIfNull(ctx);

		ProbeBuilder pb = new ProbeBuilder(C.OBSERVER_DEFINITION, C.OBSERVER_VERSION);
		pb.setStream(C.STREAM_USERINPUT_ID, C.STREAM_USERINPUT_VERSION);

		JSONObject data = new JSONObject();

		try {
			data.put(C.STREAM_USERINPUT_DATE, getDateISO8601Day(timestampNow));
			data.put(C.STREAM_USERINPUT_TEXT, text);
			data.put(C.STREAM_USERINPUT_APP, app);
		} catch (JSONException e) {
			e.printStackTrace();
			return ReturnState.JSON_EXCEPTION;
		}
		pb.setData(data.toString());
		pb.withId().now();

		ProbeWriter var = new ProbeWriter(ctx);
		try {
			pb.write(var);
			var.close();
		} catch (RemoteException e) {
			e.printStackTrace();
			return ReturnState.OHMAGE_REMOTE_EXCEPTION;
		}

		return ReturnState.ALL_OK;
	}

	public static String getDateISO8601Day(long timestamp) {
		/*
		 * The most useful non-localized pattern is "yyyy-MM-dd HH:mm:ss.SSSZ",
		 * which corresponds to the ISO 8601 international standard date format.
		 */
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd 00:00:00.SSSZ", Locale.US);
		return df.format(new Date());
	}

	public static String getDateISO8601Seconds(long timestamp) {
		/*
		 * The most useful non-localized pattern is "yyyy-MM-dd HH:mm:ss.SSSZ",
		 * which corresponds to the ISO 8601 international standard date format.
		 */
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ", Locale.US);
		return df.format(new Date());
	}

	public static void throwIfNull(Object o) {
		if (o == null) {
			throw new RuntimeException("Object can not be null ");
		}
	}
	
//	/**
//	 * Returns a token from an ohmage server. Run on a nun UI thread.
//	 * @param ctx
//	 * @param server
//	 * @param user
//	 * @param passwd
//	 * @return
//	 */
//	@Deprecated
//	public static String getToken(Context ctx, String server, String user, String passwd){
//		org.ohmage.logprobe.LogProbe.setLevel(false, Loglevel.NONE);
//		OhmageApi api = new OhmageApi(ctx);
//		AuthenticateResponse resp = api.authenticateToken("http://192.168.178.78/", "user", "useruser.", "piap");
//		//String result = "";
//		//result += "\n" + "Token: " + ;
//		//result += "\n" + "Has autherror: " + resp.hasAuthError();
//		//result += "\n" + "Result: " + resp.getResult();
//		
//		return resp.getToken();
//	}
}
